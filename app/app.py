from flask import Flask, request, abort
from urllib.parse import urlencode
import requests
import json, os

PROJECTS_API = "https://gitlab.com/api/v4/projects/"
TOKEN = "<token>"


def thankyou(json):
  payload = {'PRIVATE-TOKEN': TOKEN}
  url_str = "".join([PROJECTS_API,
                str(json['object_attributes']['project_id']),
                "/issues/",
                str(json['object_attributes']['iid']),
                "/notes?body=Thanks for creating a new issue."]
                )
  response = requests.post(url_str, headers=payload)
  return str(response) 

app = Flask(__name__)

@app.route('/comment', methods=['POST'])
def comment():
  if request.method == 'POST':
    data = request.json
    notes_response = thankyou(data)
    return notes_response
  else:
    return "NOT OK"

if __name__ == '__main__':
  app.run(debug=True,host='0.0.0.0',port=int(os.environ.get('PORT', 8080)))