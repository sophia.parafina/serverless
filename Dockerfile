FROM python:3.7

LABEL maintainer="Sophia Parafina"

COPY ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt
COPY ./app/* /app

EXPOSE 8080
CMD exec gunicorn --bind :8080 --workers 1 --threads 8 app:app
