# Serverless

This is a serverless function that comments whenever an issue is created in a project. The function is called by a Gitlab webhook, which thanks the person who created an issue using the Gitlab Notes API.

## Requirements and Assumptions

### Requirements
- Python 3
- Docker Desktop for local development
- Google Cloud Platform account
- A container registry account, e.g. Dockerhub
- A git repository 

### Assumptions
- familiarity with Python
- familiarity with Docker and containers
- familiarity with Kubernetes and Knative

## Application

The application is written in Python. Flask is used to receive the data from the webhook. The application is served with gunicorn which is a WSGI server able to handle more requests than Flask. The function can be called by multiple clients without a decrease in performance.

## Docker

The application is deployed as a container. For development and testing, the image can be built locally.

```
docker build -t commenter .
```

To test the client:

```
curl -X POST \
-H "PRIVATE-TOKEN: <token>"\
-H "Content-type: application/json"
-d "{ "object_attributes": {"iid": <issue id>,"project_id": <project id>}}"
```

## Deploying on Knative on Google Cloud Platform

Login into [Google Cloud Console](https://console.cloud.google.com/) and create a project. Click on the shell icon to open a cloud shell.

![](./images/console.png)

[Create a Kubernetes cluster and install Knative](https://github.com/knative/docs/blob/master/docs/install/Knative-with-GKE.md) using the cloud shell. Since the application will receive webhooks form Gitlab, [add an external IP](https://knative.dev/docs/serving/gke-assigning-static-ip-address/) for the serverless function.

Once kubernetes cluster created and knative is installed. Clone the serverless repository in cloud shell:

```
git clone https://gitlab.com/<user.name>/serverless.git
```

To build the container in GCP use build.yaml which builds the container and pushes it to the GCP container registry.

```
cd serverless
kubectl apply -f build.yaml
```

Check to see if the container was built.

```
kubectl get pods
```

To deploy the serverless function use service.yaml which deploys the containers in a pod.

```
kubectl apply -f service.yaml
```

Check to see if the service is running.

```
kubectl get ksvc,svc,pods,deployments,routes
```

To delete the pod that creates the container:

```
kubectl delete -f build.yaml
```

To delete the service:

```
kubectl delete -f service.yaml
```

## TODO

- implement secrets for the private token used to post to Gitlab issues
- add option to add user written message and not just canned response
- implement a way to comment on an issue without using a private token

